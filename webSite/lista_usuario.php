<?php
if(!isset($_SESSION)){
    session_start();
}
include "resources/layout/header.php"
?>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php
            include "resources/layout/menu.php";
            ?>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Lista de Usuários
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Página Inicial</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Lista dos Usuários
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class = "row">
                        <div class = "col-lg-8">
                            <div class = "form-group">
                                 <div class="mensagem sucess"></div>
                             </div>
                        </div>
                     </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <!--<th>Idade</th>-->
                                        <th>Cidade</th>
                                        <th>Estado</th>
                                        <!--<th>CEP</th>
                                        <th>Endereço</th>
                                        <th>Complemento</th>-->
                                        <th>Fazenda</th>
                                        <th>E-mail</th>
                                        <!--<th>Telefone</th>-->

                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>


    <script>
        $(document).ready(function(){
            $("li").removeClass("active");
            $("li.cadastro").addClass("active");

            function consultaAluno(){
                 $.ajax({
                    url: "data/usuarioTable.php",
                    type: "POST",
                    data: {
                        action: "select"
                    }
                }).done(function(result) {
                    result = JSON.parse(result);
                    console.log(result.data);

                    $.each(result.data, function(index, value){

                        var count = $("table > tbody > tr").length;
                        count++;
                        
                        var tr = '<tr id="usuario_' + count + '">';
                        tr += '<td id="nome_' + count + '" data-nome="' + value.nome + '">' + value.nome + '</td>';
                        //tr += '<td id="idade_' + count + '" data-idade="' + value.idade + '">' + value.idade + '</td>';
                        tr += '<td id="cidade_' + count + '" data-cidade="' + value.cidade + '">' + value.cidade + '</td>';
                        tr += '<td id="estado_' + count + '" data-estado="' + value.estado + '">' + value.estado + '</td>';
                        //tr += '<td id="cep_' + count + '" data-cep="' + value.cep + '">' + value.cep + '</td>';
                        //tr += '<td id="endereco_' + count + '" data-endereco="' + value.endereco + '">' + value.endereco + '</td>';
                        //tr += '<td id="complemento_' + count + '" data-complemento="' + value.complemento + '">' + value.complemento + '</td>';
                        tr += '<td id="fazenda_' + count + '" data-fazenda="' + value.fazenda + '">' + value.fazenda + '</td>';
                        tr += '<td id="email_' + count + '" data-email="' + value.email + '">' + value.email + '</td>';
                        //tr += '<td id="telefone_' + count + '" data-telefone="' + value.telefone + '">' + value.telefone+ '</td>';
                    
                        tr += '<td style="width:16%">';

                          // tr += '<button type="button" action = "cadastro_usuario.php" class="btn btn-primary btEditar" id="btEditar_' + value.id_usuario + '">Editar</button>';
                        tr += '<a class="btn btn-info" href = "editar_usuario.php?id_usuario=' +value.id_usuario+ '&nome=' +value.nome+ '&idade=' +value.idade+ '&cidade=' +value.cidade+ '&estado=' +value.estado+ '&cep=' +value.cep+ '&endereco=' +value.endereco+ '&complemento=' +value.complemento+ '&email=' +value.email+ '&telefone=' +value.telefone+'  ">Editar</a>';
                        tr += '<button type="button" class="btn btn-danger btDeletar" id="btDeletar_' + value.id_usuario + '">Deletar</button>';

                        tr += '</td>';
                        tr += '</tr>';

                        $("table > tbody").append(tr);
                    });
                });
            }
            
            consultaAluno();
        });

    </script>

    </body>
</html>
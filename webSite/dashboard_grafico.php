<?php
if(!isset($_SESSION)){
    session_start();
}
include "resources/layout/header.php"
?>
<body>
    
<div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php
            include "resources/layout/menu.php";
            ?>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard de Geadas
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Home</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Dashboard de Geadas
                            </li>
                        </ol>
                    </div>
                </div>
                
                <div id="mapid" style="height: 40px;"></div>

        </div>
        <!-- /#page-wrapper -->
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        $.ajax({
                url: "data/geadaTable.php",
                type: "POST",
                data: {
                    action: "getGeadasBrancas"
    
                     }

            }).done(function(result) {
                    result = JSON.parse(result);
                    console.log(result.data);
                    



                    $.each(result.data, function(index, value){
                        qtdBranca = value.tipoBranca;
                        //alert(qtdBranca);
                    });

                    $.ajax({
                        url: "data/geadaTable.php",
                        type: "POST",
                        data: {
                            action: "getGeadasNegras"
            
                            }

                    }).done(function(result) {
                            result = JSON.parse(result);
                            console.log(result.data);


                            $.each(result.data, function(index, value){
                                qtdNegra = value.tipoNegra;
                                // alert(qtdNegra);
                            });

                          
                            var data = google.visualization.arrayToDataTable([
                            ['Tipo', 'Quantidade'],
                            ['Geada Branca',     parseInt(qtdBranca)],
                            ['Geada Negra',      parseInt(qtdNegra)]
                    
                            ]);

                            var options = {
                            title: 'Tipo de Geada'
                            };

                            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                            chart.draw(data, options);
                    });

                        
                
            });

      }
    </script>
  </head>
  <body>
    <div id="piechart" style="width: 700px; height: 450px;"></div>
  </body>


    </body>
</html>
<?php
if(!isset($_SESSION)){
    session_start();
}
include "resources/layout/header.php"
?>
<body>
    
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php
            include "resources/layout/menu.php";
            ?>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard - Mapa de Alerta
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Página Inicial</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Dashboard - Mapa de Alerta
                            </li>
                        </ol>
                    </div>
                </div>
                
                <div id="mapid" style="height: 400px;"></div>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin=""/>
   <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA==" crossorigin="">
   </script>
   <script>
       var mymap = L.map('mapid').setView([-22.1075, -50.1764], 7);
       L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiY2hpY2FvNTAwIiwiYSI6ImNqbmV3NjM0ajA4ZGUzd25iZDhqbnJjejcifQ.K2W1ngsLWAmxfqzkjxv2XQ', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap);


    $(document).ready(function(){
       
            $.ajax({
                url: "data/cidadeTable.php",
                type: "POST",
                data: {
                    action: "select"
    
                     }

            }).done(function(result) {
                    result = JSON.parse(result);
                    console.log(result.data);

                    $.each(result.data, function(index, value){

                       // alert(value.latitude);
                        var marker = L.marker([value.latitude, value.longitude]).addTo(mymap);
                    });
                        
                
            });
        });

    // var marker = L.marker([-22.21, -49.94]).addTo(mymap); //MARILIA
    //          L.marker([-22.1, -50.17]).addTo(mymap); //Pompeia
    //          L.marker([-22.66, -50.41]).addTo(mymap); //Assis
   </script>
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    </body>
</html>
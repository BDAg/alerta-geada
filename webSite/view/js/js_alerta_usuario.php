<script>


    $(document).ready(function(){
        $("#btAlterar").on("click",function(){
            $.ajax({
                url: "data/usuarioTable.php",
                type: "POST",
                data: {
                    action: "alterar",
                    nome : $("#nome").val(),
                    idade: $("#idade").val(),
                    telefone: $("#telefone").val(),
                    cidade: $("#cidade").val(),
                    cep: $("#cep").val(),
                    estado: $("#estado").val(),
                    endereco: $("#endereco").val(),
                    complemento: $("#complemento").val(),
                    email: $("#email").val(),
                    id_usuario: $("#id_usuario").val()
                }
            }).done(function(data) {
                data = JSON.parse(data);
                if(data.success == true){
                    $("div.mensagem").addClass("alert-success").html("<h4>Alterado com sucesso!</h4>").show();
                }else{
                    $(".mensagem_erro").show();
                    $(".mensagem_erro").append("Não foi possivel alterar o aluno");
                }
                LimpaDados();
            });
        });
    });

    $(document).ready(function(){
        $("#nome").val('<?php echo $_GET["nome"]?>');
        $("#cidade").val('<?php echo $_GET["cidade"]?>');
        $("#estado").val('<?php echo $_GET["estado"]?>');
        $("#cep").val('<?php echo $_GET["cep"]?>');
        $("#telefone").val('<?php echo $_GET["telefone"]?>');
        $("#idade").val('<?php echo $_GET["idade"]?>');
        $("#email").val('<?php echo $_GET["email"]?>');
        $("#complemento").val('<?php echo $_GET["complemento"]?>');
        $("#endereco").val('<?php echo $_GET["endereco"]?>');
        $("#id_usuario").val('<?php echo $_GET["id_usuario"]?>');   
    });

    $(document).ready(function(){


        $("#idade").on("keyup",function(){
            var idade = this.value;
            this.value = idade.replace(/[^\d]+/g,'');
        });
    });

    $(document).ready(function(){
        $("#btSalvar").on("click",function(){
            limparCampos();
            var id_usuario = $("#id_usuario").val();
            var nome    = $("#nome").val();
            var idade   = $("#idade").val();
            var telefone   = $("#telefone").val();
            var cidade   = $("#cidade").val();
            var cep   = $("#cep").val();
            var estado   = $("#estado").val();
            var endereco   = $("#endereco").val();
            var complemento   = $("#complemento").val();
            var email   = $("#email").val();
            var senha   = $("#senha").val();
            var confsenha = $("#confsenha").val();
            var nome_fazenda = $("#nome_fazenda").val();
            var cep_fazenda = $("#cep_fazenda").val();
            var cidade_fazenda = $("#cidade_fazenda").val();
            var estado_fazenda = $("#estado_fazenda").val();
            var last_id_usuario;
            var last_id_cidade;
            var last_id_fazenda;
            var erro = false;

            if(nome.length == 0){
                $("#nome").css("border-color","red");
                $("#nome").css("color","red");
                erro = true;
            }
            if(idade.length == 0){
                $("#idade").css("border-color","red");
                $("#idade").css("color","red");
                erro = true;
                }
            if(telefone.length == 0){
                $("#telefone").css("border-color","red");
                $("#telefone").css("color","red");
                erro = true;
            }
            if(cidade.length == 0){
                $("#cidade").css("border-color","red");
                $("#cidade").css("color","red");
                erro = true;
            }
            if(cep.length == 0){
                $("#cep").css("border-color","red");
                $("#cep").css("color","red");
                erro = true;
            }
            if(estado.length == 0){
                $("#estado").css("border-color","red");
                $("#estado").css("color","red");
                erro = true;
            }
            if(endereco.length == 0){
                $("#endereco").css("border-color","red");
                $("#endereco").css("color","red");
                erro = true;
            }
            if(complemento.length == 0){
                $("#complemento").css("border-color","red");
                $("#complemento").css("color","red");
                erro = true;
            }
            if(email.length == 0){
                $("#email").css("border-color","red");
                $("#email").css("color","red");
                erro = true;
            }
            if ($("#id_usuario").val().length == 0 ){
                if(senha.length == 0){
                    $("#senha").css("border-color","red");
                    $("#senha").css("color","red");
                    erro = true;
                }
                if(confsenha.length == 0){
                    $("#confsenha").css("border-color","red");
                    $("#confsenha").css("color","red");
                    erro = true;
                }

                if(nome_fazenda.length == 0){
                    $("#nome_fazenda").css("border-color","red");
                    $("#nome_fazenda").css("color","red");
                    erro = true;
                }
                if(cep_fazenda.length == 0){
                    $("#cep_fazenda").css("border-color","red");
                    $("#cep_fazenda").css("color","red");
                    erro = true;
                }
                if(cidade_fazenda.length == 0){
                    $("#cidade_fazenda").css("border-color","red");
                    $("#cidade_fazenda").css("color","red");
                    erro = true;
                    }
                if(estado_fazenda.length == 0){
                    $("#estado_fazenda").css("border-color","red");
                    $("#estado_fazenda").css("color","red");
                    erro = true;
                }
            }

            if(erro == true){
                $("div.mensagem").addClass("alert-danger").html("<h4>Preencha todos os campos</h4>").show();
            }else{
                $.ajax({
                    url: "data/usuarioTable.php",
                    type: "POST",
                    data: {
                    action: "inserir",
                        nome: nome,
                        email: email,
                        endereco: endereco,
                        cidade: cidade,
                        idade: idade,
                        telefone: telefone,
                        cep: cep,
                        senha: senha, 
                        complemento: complemento,
                        estado: estado,
                        telefone: telefone
                    }
                }).done(function(data) {
                    data = JSON.parse(data);
                    if(data.success == true){
                        //$("div.mensagem").addClass("alert-success").html("<h4>Cadastrado com sucesso!</h4>").show();
                        last_id_usuario = data.id_usuario;
                $.ajax({
                    url: "data/cidadeTable.php",
                    type: "POST",
                    data: {
                    action: "inserir",
                        nome: cidade_fazenda

                    }
                }).done(function(data) {
                    
                    data = JSON.parse(data);
                    if(data.success == true){
                        
                        //$("div.mensagem").addClass("alert-success").html("<h4>Cadastrado com sucesso!</h4>").show();
                        last_id_cidade = data.id_cidade;
                        
                        // alert(last_id_usuario);
                        
                $.ajax({
                    url: "data/fazendaTable.php",
                    type: "POST",
                    data: {
                    action: "inserir",
                        nome: nome_fazenda,
                        id_usuario: last_id_usuario,
                        id_cidade: last_id_cidade

                    }
                }).done(function(data) {
                    data = JSON.parse(data);
                    if(data.success == true){
                        //$("div.mensagem").addClass("alert-success").html("<h4>Cadastrado com sucesso!</h4>").show();
                        last_id_fazenda = data.id_fazenda;                      
                    }else{
                        $(".mensagem_erro").show();
                        $(".mensagem_erro").append("Erro ao cadastrar fazenda!");
                        setTimeout(function(){window.location = "cadastro_usuario.php";}, 3000);
                    }
                    //LimpaDados();
                });                        
                    }else{
                        $(".mensagem_erro").show();
                        $(".mensagem_erro").append("Erro ao cadastrar cidade!");
                        setTimeout(function(){window.location = "cadastro_usuario.php";}, 3000);
                    }
                    //LimpaDados();
                });				
                    }else{
                        $(".mensagem_erro").show();
                        $(".mensagem_erro").append("Erro ao cadastrar usuário!");
                        setTimeout(function(){window.location = "cadastro_usuario.php";}, 3000);
                    }
                    LimpaDados();
                    $("div.mensagem").addClass("alert-success").html("<h4>Cadastrado com sucesso!</h4>").show();
                });

                
                
                

            }
            
        });
    });

    $(document).ready(function(){
        $("#telefone").bind('input propertychange',function(){
            var texto = $(this).val();
            texto = texto.replace(/[^\d]/g, '');
            if (texto.length > 0){
            texto = "(" + texto;
                if (texto.length > 3){
                    texto = [texto.slice(0, 3), ") ", texto.slice(3)].join('');  
                }
                if (texto.length > 12){      
                    if (texto.length > 13) 
                        texto = [texto.slice(0, 10), "-", texto.slice(10)].join('');
                    else
                        texto = [texto.slice(0, 9), "-", texto.slice(9)].join('');
                }   
                if (texto.length > 15)                
                    texto = texto.substr(0,15);
            }
            $(this).val(texto);     
        });
    });

    $(document).on("click","button.btDeletar",function(){
            var id_usuario = this.id.replace(/[^\d]+/g,'');
            if(confirm("Deseja realmente deletar este usuário?")){
                $.ajax({
                    url: "data/usuarioTable.php",
                    type: "POST",
                    data: {
                        action: "deletar",
                        id_usuario: id_usuario
                        
                    }
                }).done(function(data) {
                    data = JSON.parse(data);
                    if(data.success == true){
                        alert('Usuário excluído com sucesso!');
                        location.reload();
                       // $("div.mensagem").addClass("alert-success").html("<h4>Excluido com sucesso!</h4>").show();
                    }else{
                        $(".mensagem_erro").show();
                        $(".mensagem_erro").append("Não foi possivel excluir o registro");
                    }
                });
                $("#usuario_"+id_usuario).remove();
            }
        });
    

    function limparCampos(){
        $("#nome").css("border-color","");
        $("#nome").css("color","");

        $("#idade").css("border-color","");
        $("#idade").css("color","");

        $("#telefone").css("border-color","");
        $("#telefone").css("color","");
        
        $("#email").css("border-color","");
        $("#email").css("color","");

        $("#cidade").css("border-color","");
        $("#cidade").css("color","");

        $("#estado").css("border-color","");
        $("#estado").css("color","");

        $("#cep").css("border-color","");
        $("#cep").css("color","");

        $("#endereco").css("border-color","");
        $("#endereco").css("color","");

        $("#complemento").css("border-color","");
        $("#complemento").css("color","");

        $("#senha").css("border-color","");
        $("#senha").css("color","");

        $("#confsenha").css("border-color","");
        $("#confsenha").css("color","");

        $("#nome_fazenda").css("border-color","");
        $("#nome_fazenda").css("color","");

        $("#cep_fazenda").css("border-color","");
        $("#cep_fazenda").css("color","");

        $("#cidade_fazenda").css("border-color","");
        $("#cidade_fazenda").css("color","");

        $("#estado_fazenda").css("border-color","");
        $("#estado_fazenda").css("color","");

        $("div.mensagem").removeClass("alert-danger  alert-success").html("").hide();
    }
		
	function LimpaDados(){
		$("#id_aluno").val("");
		$("#nome").val("");
		$("#idade").val("");
        $("#telefone").val("");
        $("#cidade").val("");
        $("#estado").val("");
        $("#cep").val("");
        $("#endereco").val("");
        $("#complemento").val("");
        $("#senha").val("");
        $("#confsenha").val("");
	    $("#email").val("");
        $("#nome_fazenda").val("");
        $("#cep_fazenda").val("");
        $("#cidade_fazenda").val("");
        $("#estado_fazenda").val("");
        
	}



</script>
<?php 
	class DbConnection{
		PRIVATE $conn = '';
		
		public function setConnection($hostname,$username,$pwd,$db){
			$this->conn = new mysqli($hostname, $username,$pwd, $db);
		}

		public function getConnection(){
			return $this->conn;
		}
		
		public function closedConnection(){
			mysqli_close($this->conn);
		}
	}
?>
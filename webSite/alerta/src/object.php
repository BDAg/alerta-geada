<?php
	class ObjectMailer{
		public function prepareSender($para,$cidade,$pathMail,$mailer){
			$mailer->isSMTP();
			$mailer->SMTPAuth = true;
			$mailer->SMTPSecure = 'tls';

			$mailer->Host = 'smtp.gmail.com';
			$mailer->SMTP_PORT = "587";
			$mailer->SMTPDebug = 0;
			$mailer->Username = 'alerta.geada@gmail.com';
			$mailer->Password = 'alerta123';

			$mailer->From = 'alerta.geada@gmail.com';
			$mailer->FromName = "Frost Advisor";

			$mailer->Subject = '[Frost Advisor] Alerta de Geada';
			$mailer->MsgHTML(str_replace('{cidade}',$cidade,file_get_contents($pathMail)));
			$mailer->AddAddress($para);
		}
		
		public function sendMail($mailer){
			if($mailer->Send()){
				http_response_code(200);
			} else {
				http_response_code(400);
			}
			
			return http_response_code();
		}
	}

?>
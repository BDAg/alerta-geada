<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();		
	} else {
		if(!isset($_SESSION['id_usuario'])){
			exit;
		}
	}
?>
            <div class="navbar-header">
                <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button> -->
                <a class="navbar-brand" href="index.php">Frost Advisor</a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <?php if($_SESSION["id_usuario"]){ ?>
                        <li>
                        <a href="index.php"><i class="fa fa-fw fa-dashboard"></i><img src="https://img.icons8.com/small/16/000000/top-menu.png"> Página Inicial</a>
                    </li>
                    <?php } ?>

                    <li class="cadastro">
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i><img src="https://img.icons8.com/small/16/000000/menu.png"> Editar Perfil<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li class="edtperfil">
                                <a href="alterar_senha.php"><img src="https://img.icons8.com/small/16/000000/menu-2.png">Alterar Senha</a>
                                <a href=""><img src="https://img.icons8.com/small/16/000000/menu-2.png">Alterar Informações</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                    <a href="lista_usuario.php"><i class="fa fa-fw fa-dashboard"></i><img src="https://img.icons8.com/small/16/000000/menu.png"> Lista de Usuários</a>
                    </li>

                    <li>
                        <a href="geadas_anteriores.php"><i class="fa fa-fw fa-dashboard"></i><img src="https://img.icons8.com/small/16/000000/menu.png"> Geadas Anteriores</a>
                    </li>
                    <li>
                        <a href="dashboard_mapa.php"><i class="fa fa-fw fa-dashboard"></i><img src="https://img.icons8.com/small/16/000000/menu.png"> Mapa de Geadas</a>
                    </li>
                    <li>
                        <a href="dashboard_grafico.php"><i class="fa fa-fw fa-dashboard"></i><img src="https://img.icons8.com/small/16/000000/menu.png"> Gráfico de Geadas</a>
                    </li>
                    <!-- <li>
                        <a href="cadastro_fazenda.php"><i class="fa fa-fw fa-dashboard"></i>Cadastro Fazenda</a>
                    </li> -->
                    <li>


                    <br><br><a href="data/logout.php"><i class="fa fa-fw fa-dashboard"></i><img src="https://img.icons8.com/small/16/000000/logout-rounded.png"> Sair</a>
                    </li>
                </ul>
            </div>

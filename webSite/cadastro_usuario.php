<?php
    include "resources/layout/header.php"
?>
    <script>
        function meu_callback(conteudo) {
            if (!("erro" in conteudo)) {
                document.getElementById('cidade').value=(conteudo.localidade);
                document.getElementById('estado').value=(conteudo.uf);
            }else{
                alert("CEP não encontrado.");
            }
        };
        
        function pesquisacep(valor) {
            var cep = valor.replace(/\D/g, '');
            var validacep = /^[0-9]{8}$/;
            var script = document.createElement('script');
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
            document.body.appendChild(script);
        };
    </script>

    <body>
        <div id="wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"></nav>
            <div id="page-wrapper">
                <div class="container-fluid">

                    <div class="col-lg-8">

                        <div class="row">
                            <h1 class="page-header">Cadastro Usuário</h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="login.php">Login</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-edit"></i> Cadastro Usuário
                                </li>
                            </ol>
                        </div>
                    </div>


                    <form role="form">
                        <div class = "row">
                            <div class = "col-lg-8">
                                <div class = "form-group">
                                    <div class="mensagem"></div>
                                </div>
                            </div>
                        </div>
                            
                        <div class="row">
                            <input type="hidden" name="id_usuario" id="id_usuario" value=""/>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <label>Nome</label>
                                    <input class="form-control" name="nome" id="nome" placeholder="Informe o seu nome">
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label>Idade</label>
                                    <input class="form-control" name="idade" id="idade" maxlength="3" placeholder="">
                                </div>
                            </div>
                        </div>
                            
                        <div class="row">    
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Telefone</label>
                                    <input class="form-control" name="telefone" id="telefone" placeholder="Informe seu telefone" type="phone">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>CEP</label>
                                    <input onblur="pesquisacep(this.value);" class="form-control" name="cep" id="cep" maxlength="8" placeholder="Informe seu CEP" type="text">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Cidade</label>
                                    <input disabled="true" class="form-control" name="cidade" id="cidade" placeholder="" type="city">
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label>Estado</label>
                                    <input disabled="true" class="form-control" name="estado" id="estado" maxlength="2" placeholder="" type="state">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Endereço</label>
                                    <input class="form-control" name="endereco" id="endereco" placeholder="Informe seu endereço" type="email">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Complemento</label>
                                    <input class="form-control" name="complemento" id="complemento" placeholder="Informe o complemento" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input class="form-control" name="email" id="email" placeholder="Informe seu e-mail" type="email">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class = "col-lg-8">
                                <div class = "form-group">
                                    <label>Senha</label>
                                    <input class= "form-control"name="senha"id= "senha" placeholder="Informe sua senha", type = "password">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class = "col-lg-8">
                                <div class = "form-group">
                                    <label>Confirmar Senha</label>
                                    <input class= "form-control"name="confsenha"id= "confsenha" placeholder="Confirme sua senha", type = "password">
                                </div>
                            </div>
                        </div>

                        
                        <script>
                            function meu_callback2(conteudo) {
                                if (!("erro" in conteudo)) {
                                    document.getElementById('cidade_fazenda').value=(conteudo.localidade);
                                    document.getElementById('estado_fazenda').value=(conteudo.uf);
                                }else{
                                    alert("CEP não encontrado.");
                                }
                            };
                                
                            function pesquisacep2(valor) {
                                var cep = valor.replace(/\D/g, '');
                                var validacep = /^[0-9]{8}$/;
                                var script = document.createElement('script');
                                script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback2';
                                document.body.appendChild(script);
                            };
                                </script>      

                                <div class="col-lg-8">
                                    <div class="row">
                                        <h1 class="page-header">Cadastro da Fazenda</h1>
                                    </div>
                                </div>

                                <div class="row">
                                    <input type="hidden" name="id_fazenda" id="id_fazenda" value=""/>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label>Nome da Fazenda</label>
                                            <input class="form-control" name="nome_fazenda" id="nome_fazenda" placeholder="Informe o nome da fazenda">
                                        </div>
                                    </div>
                                </div>
                                                    
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>CEP da Fazenda</label>
                                            <input maxlength="8" onblur="pesquisacep2(this.value);" class="form-control" name="cep_fazenda" id="cep_fazenda" placeholder="Informe o CEP da fazenda" type="cep">
                                        </div>
                                    </div>
                                    <div class = "col-lg-4">
                                        <div class = "form-group">
                                            <label>Cidade</label>
                                            <input disabled="true" class= "form-control"name="cidade_fazenda"id= "cidade_fazenda" placeholder="", type = "text">
                                        </div>
                                    </div>
                                    <div class = "col-lg-1">
                                        <div class = "form-group">
                                            <label>Estado</label>
                                            <input disabled="true" class= "form-control"name="estado_fazenda"id= "estado_fazenda" placeholder="", type = "text">
                                        </div>
                                    </div>
                                </div>

                                <div class = "row">
                                    <div class = "col-lg-5">
                                        <div class = "form-group">
                                        </div>
                                    </div>
                                    <div class = "col-lg-1">
                                        <div class = "form-group">
                                            <!-- <button type="button" class="btn btn-success" id="btAddFazenda">Adicionar Mais Fazendas</button>   -->
                                        </div>
                                    </div>
                                </div>

                                <div class = "row">
                                    <div class = "col-lg-1">
                                        <div class = "form-group">
                                            <button type="button" class="btn btn-success" id="btSalvar">Salvar</button>
                                            
                                        </div>
                                    </div>
                                    <a class="btn btn-info" href ="login.php">Voltar</a>
                                </div>
 
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
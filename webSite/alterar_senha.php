<?php
if(!isset($_SESSION)){
    session_start();
}
include "resources/layout/header.php"
?>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
                  <?php
            include "resources/layout/menu.php";
            ?>
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                
                <div class="col-lg-8">
                    <div class="row">
                        <h1 class="page-header">
                            Alterar Senha
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Página Inicial</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Alterar Senha
                            </li>
                        </ol>
                    </div>
                </div>


                <form action="./alteracao-senha/resources.php" method="post" id="alteracao-senha">
                    <div class = "row">
                        <div class = "col-lg-8">
                            <div class = "form-group">
                                 <div class="mensagem"></div>
                             </div>
                        </div>
                     </div>

                    <div class = "row">
                        <div class = "col-lg-5">
                                <div class = "form-group">
                                    <label>Senha Atual</label>
                                    <input class= "form-control"name="senha"id= "senha" placeholder="Informe sua senha atual", type = "password">
                                </div>
                            </div>
                    </div>
                    <br>
                    <div class = "row">
                            <div class = "col-lg-5">
                                <div class = "form-group">
                                    <label>Nova Senha</label>
                                    <input class= "form-control"name="newsenha"id= "newsenha" placeholder="Informe sua nova senha", type = "password">
                                </div>
                            </div>
                    </div>
                    <div class = "row">
                            <div class = "col-lg-5">
                                <div class = "form-group">
                                    <label>Confirme a nova senha</label>
                                    <input class= "form-control"name="confnewsenha"id= "confnewsenha" placeholder="Confirme sua nova senha", type = "password">
                                     <br><button type="button" class="btn btn-success" id="btnAlterarSenha">Salvar</button> 
                                     <!-- <br><input class = "btn btn-sucess" type = "submit" value = "Trocar Senha"> </> -->
                                </div>
                               
                            </div>
                    </div>

                    
                </form>

               <script>
        $("#btnAlterarSenha").click(function(){
            $("li").removeClass("active");
            $("li.cadastro").addClass("active");
			
            function trocaSenha(){
                 $.ajax({
                    url: "data/usuarioTable.php",
                    type: "POST",
                    data: {
                        action: "alterarSenha",
						oldsenha: $("#senha").val(),
						senha: $("#newsenha").val()
						
                    }
                }).done(function(result) {
					
                    result = JSON.parse(result);
					console.log(result.data)
					
                });
            }
			
            var senha   = $("#senha").val();
			var newsenha   = $("#newsenha").val();
            var confnewsenha = $("#confnewsenha").val();
            var erro_confsenha = false;
            var erro_report = false;


            if(senha.length == 0){
                    $("#senha").css("border-color","red");
                    $("#senha").css("color","red");
                    erro_report = true;
                }
            if(newsenha.length == 0){
                    $("#newsenha").css("border-color","red");
                    $("#newsenha").css("color","red");
                    erro_report = true;
                }
            if(confnewsenha.length == 0){
                    $("#confnewsenha").css("border-color","red");
                    $("#confnewsenha").css("color","red");
                    erro_report = true;
                }
            
            if(newsenha != confnewsenha){
                
                erro_confsenha = true;
                
            }
            
			if( erro_report == true){
                $("div.mensagem").addClass("alert-danger").html("<h4>Preencha todos os campos</h4>").show();
            }
            if( erro_confsenha == true){
                $("div.mensagem").addClass("alert-danger").html("<h4>As senhas não conferem!</h4>").show();

            }
            if (erro_report == false && erro_confsenha == false){
				trocaSenha();
                $("div.mensagem").addClass("alert-success").html("<h4>Senha alterada com sucesso!</h4>").show();
                $("#senha").val("");
                $("#newsenha").val("");
                $("#confnewsenha").val("");

            }
           
      
        });
        
    </script>
    

    </body>
</html>
<?php
if(!isset($_SESSION)){
    session_start();
}
include "resources/layout/header.php"
?>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php
            include "resources/layout/menu.php";
            ?>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Geadas Anteriores
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Página Inicial</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Geadas Anteriores
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Cidade</th>
                                        <th>Data / Hora</th>
                                        <th>Umidade(%)</th>
                                        <th>Temperatura - MAX / MIN (ºC)</th>
                                        <th>Vel. do Vento(km/h)</th>
                                        <th>Tipo</th>
                                        <th>Motivo</th>


                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>


    <script>
        $(document).ready(function(){
            $("li").removeClass("active");
            $("li.cadastro").addClass("active");

            function consultaGeada(){
                 $.ajax({
                    url: "data/geadaTable.php",
                    type: "POST",
                    data: {
                        action: "select"
                    }
                }).done(function(result) {
                    result = JSON.parse(result);
                    console.log(result.data);

                    $.each(result.data, function(index, value){

                        var count = $("table > tbody > tr").length;
                        count++;
                        
                        var tr = '<tr id="geada_' + count + '">';
                        tr += '<td id="cidade_' + count + '">' + value.nome + '</td>';
                        tr += '<td id="data_' + count + '">' + value.horadata + '</td>';
                        tr += '<td id="umidade_' + count + '">' + value.umidade + '</td>';
                        tr += '<td id="temperatura_' + count + '">' + value.maximo + ' / ' + value.minima +'</td>';
                        tr += '<td id="vento_vel_' + count + '">' + value.vento_vel + '</td>';
                        tr += '<td id="tipo_' + count + '">' + value.tipo + '</td>'
                        tr += '<td id="motivo_' + count + '">' + value.motivo + '</td>';

                    
                        tr += '<td style="width:16%">';
                        tr += '</td>';
                        tr += '</tr>';

                        $("table > tbody").append(tr);
                        
                    });
                });
            }
            
            consultaGeada();
        });

    </script>


    </body>
</html>
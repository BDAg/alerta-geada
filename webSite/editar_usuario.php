<?php
include "resources/layout/header.php"
?>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
         
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                
                <div class="col-lg-8">
                    <div class="row">
                        <h1 class="page-header">
                            Editar Usuário
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Página Inicial</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Editar Usuário
                            </li>
                        </ol>
                    </div>
                </div>


                <form role="form">

                      <div class = "row">
                                <div class = "col-lg-8">
                                    <div class = "form-group">
                                        <div class="mensagem"></div>
                                    </div>
                                </div>
                            </div>
                        
                        <div class="row">

                            <input type="hidden" name="id_usuario" id="id_usuario" value=""/>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <label>Nome</label>
                                    <input class="form-control" name="nome" id="nome" placeholder="Informe o seu nome">
                                </div>
                            </div>
                            <div class="col-lg-1">
                            <div class="form-group">
                                <label>Idade</label>
                                <input class="form-control" name="idade" id="idade" maxlength="3" placeholder="">
                            </div>
                        </div>
                        </div>
                         
                    <div class="row">
                        
                        <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Telefone</label>
                                    <input class="form-control" name="telefone" id="telefone" placeholder="Informe seu telefone" type="phone">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Cidade</label>
                                    <input class="form-control" name="cidade" id="cidade" placeholder="Informe sua cidade" type="city">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>CEP</label>
                                    <input class="form-control" name="cep" id="cep" placeholder="Informe seu cep" type="text">
                                </div>
                            </div>

                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label>Estado</label>
                                    <input class="form-control" name="estado" id="estado" placeholder="" type="state">
                                </div>
                            </div>
                    </div>
                    <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Endereço</label>
                                    <input class="form-control" name="endereco" id="endereco" placeholder="Informe seu endereço" type="email">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Complemento</label>
                                    <input class="form-control" name="complemento" id="complemento" placeholder="Informe o complemento" type="text">
                                </div>
                            </div>
                    </div>
                    <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input class="form-control" name="email" id="email" placeholder="Informe seu e-mail" type="email">
                                </div>
                            </div>
                    </div>

                            <div class = "row">
                                <div class = "col-lg-8">
                                <div class = "form-group">
                                    <button type="button" class="btn btn-success" id="btAlterar">Salvar</button>
                                    <a class="btn btn-info" href ="index.php">Voltar</a>
                                    </div>
                            </div>
                            </div>

                                
                            </div>
                    </div>
                        
                    </form>
             
            </div>
        </div>
    </div>
</body>
</html>
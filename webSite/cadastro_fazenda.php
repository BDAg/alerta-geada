<?php
    include "resources/layout/header.php"
?>

    <script>
        function meu_callback2(conteudo) {
            if (!("erro" in conteudo)) {
                document.getElementById('cidade_fazenda').value=(conteudo.localidade);
                document.getElementById('estado_fazenda').value=(conteudo.uf);
            }else{
                alert("CEP não encontrado.");
            }
        };
            
        function pesquisacep2(valor) {
            var cep = valor.replace(/\D/g, '');
            var validacep = /^[0-9]{8}$/;
            var script = document.createElement('script');
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback2';
            document.body.appendChild(script);
        };
    </script>      

    <div class="col-lg-8">
        <div class="row">
            <h1 class="page-header">Cadastro da Fazenda</h1>
        </div>
    </div>

    <div class="row">
        <input type="hidden" name="id_fazenda" id="id_fazenda" value=""/>
        <div class="col-lg-8">
            <div class="form-group">
                <label>Nome da Fazenda</label>
                <input class="form-control" name="nome_fazenda" id="nome_fazenda" placeholder="Informe o nome da fazenda">
            </div>
        </div>
    </div>
                         
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                <label>CEP da Fazenda</label>
                <input maxlength="8" onblur="pesquisacep2(this.value);" class="form-control" name="cep_fazenda" id="cep_fazenda" placeholder="Informe o CEP da fazenda" type="cep">
            </div>
        </div>
        <div class = "col-lg-4">
            <div class = "form-group">
                <label>Cidade</label>
                <input disabled="true" class= "form-control"name="cidade_fazenda"id= "cidade_fazenda" placeholder="", type = "text">
            </div>
        </div>
        <div class = "col-lg-1">
            <div class = "form-group">
                <label>Estado</label>
                <input disabled="true" class= "form-control"name="estado_fazenda"id= "estado_fazenda" placeholder="", type = "text">
            </div>
        </div>
    </div>

    <div class = "row">
        <div class = "col-lg-5">
            <div class = "form-group">
            </div>
        </div>
        <div class = "col-lg-1">
            <div class = "form-group">
                <!-- <button type="button" class="btn btn-success" id="btAddFazenda">Adicionar Mais Fazendas</button>   -->
            </div>
        </div>
    </div>

    <div class = "row">
        <div class = "col-lg-1">
            <div class = "form-group">
                <button type="button" class="btn btn-success" id="btEoq">Salvar</button>  
            </div>
        </div>
    </div>


<?php

session_start();

require_once 'Base.php';

class Geada extends Base {

    public function select() {
        $data = (object) $_POST;

        $db = $this->getDb();
        $stm = $db->prepare('SELECT Alerta.id_alerta, Alerta.id_fazenda, Alerta.id_clima, DATE_FORMAT(STR_TO_DATE(horadata, "%Y-%m-%d %H:%i"), "%d/%m/%Y %H:%i") as horadata, Alerta.motivo, Alerta.tipo, Climas.id_clima, Climas.maximo, Climas.minima, Climas.vento_vel, Climas.umidade, Climas.id_cidade, Cidades.nome, Cidades.id_cidade FROM Alerta INNER JOIN Climas ON Alerta.id_clima = Climas.id_clima INNER JOIN Cidades ON Climas.id_cidade = Cidades.id_cidade');
        $stm->execute();
        $result = $stm->fetchAll( PDO::FETCH_ASSOC);


        echo json_encode(array(
            "data" => $result,
            "success" => true
            )
        );

    }

    public function getGeadasBrancas() {
        $data = (object) $_POST;

        $db = $this->getDb();
        $stm = $db->prepare('SELECT COUNT(tipo) as tipoBranca FROM Alerta WHERE tipo = "Branca"');
        $stm->execute();
        $result = $stm->fetchAll( PDO::FETCH_ASSOC);


        echo json_encode(array(
            "data" => $result,
            "success" => true
            )
        );

    }

    public function getGeadasNegras() {
        $data = (object) $_POST;

        $db = $this->getDb();
        $stm = $db->prepare('SELECT COUNT(tipo) as tipoNegra FROM Alerta WHERE tipo = "Negra"');
        $stm->execute();
        $result = $stm->fetchAll( PDO::FETCH_ASSOC);


        echo json_encode(array(
            "data" => $result,
            "success" => true
            )
        );

    }
}




$acao = $_POST["action"];
$geada = new Geada();
$geada->$acao();
?>
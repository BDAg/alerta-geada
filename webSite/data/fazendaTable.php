<?php

session_start();

require_once 'Base.php';

class Fazenda extends Base{

    public function inserir() {
        $data = (object) $_POST;

        $db = $this->getDb();
        $stm = $db->prepare('INSERT INTO Fazendas (nome, id_usuario, id_cidade) values (:nome, :id_usuario, :id_cidade) ');

		$stm->bindValue(':nome',  $data->nome);
        $stm->bindValue(':id_usuario',  $data->id_usuario);
        $stm->bindValue(':id_cidade',  $data->id_cidade);

 
        $stm->execute();
		$lastId = $db->lastInsertId();
		$result = $stm;
		
		if($result->rowCount()){
            $success = true;
        }else{
            $success = false;
        }
		
		echo json_encode(array(

            "data" => $result,
			"id_fazenda" => $lastId, 
            "success" => true
            )
        );
    }

}

$acao = $_POST["action"];

$fazenda = new Fazenda();
$fazenda->$acao();
?>
<?php

session_start();

require_once 'Base.php';

class Aluno extends Base{

    public function select() {
        $data = (object) $_POST;

        $db = $this->getDb();
        $stm = $db->prepare('SELECT Usuarios.id_usuario, Usuarios.nome, Usuarios.idade, Usuarios.telefone,Usuarios.cidade, Usuarios.endereco, Usuarios.cep, Usuarios.estado, Usuarios.complemento, Usuarios.email, Usuarios.senha, Fazendas.nome as fazenda FROM Usuarios INNER JOIN Fazendas WHERE Fazendas.id_usuario = Usuarios.id_usuario');
        $stm->execute();
        $result = $stm->fetchAll( PDO::FETCH_ASSOC);

        // foreach ($result as $key => $value) {
        //     $result[$key]["nome"] = utf8_encode($result[$key]["nome"]);
        //     $result[$key]["curso"] = utf8_encode($result[$key]["curso"]);
        // }

        echo json_encode(array(
            "data" => $result,
            "success" => true
            )
        );
        
    }
    public function VerificaSenha() {
        $data = (object) $_POST;

        $db = $this->getDb();
        $stm = $db->prepare('SELECT Usuarios.email FROM Usuarios WHERE email = :email');

        $stm->bindValue(':email',  $data->email);

        $stm->execute();
        $result = $stm->fetchAll( PDO::FETCH_ASSOC);

        // foreach ($result as $key => $value) {
        //     $result[$key]["nome"] = utf8_encode($result[$key]["nome"]);
        //     $result[$key]["curso"] = utf8_encode($result[$key]["curso"]);
        // }

        echo json_encode(array(
            "data" => $result,
            "success" => true
            )
        );
        
    }
	
	public function inserir() {
        $data = (object) $_POST;

        $db = $this->getDb();
        $stm = $db->prepare('INSERT INTO Usuarios (nome, idade, telefone, cidade, cep, estado, endereco, complemento, email, senha) values (:nome, :idade, :telefone, :cidade, :cep, :estado, :endereco, :complemento, :email, :senha) ');

		$stm->bindValue(':nome',  $data->nome);
        $stm->bindValue(':idade',  $data->idade);
        $stm->bindValue(':telefone',  $data->telefone);
        $stm->bindValue(':cidade',  $data->cidade);
        $stm->bindValue(':cep',  $data->cep);
        $stm->bindValue(':estado',  $data->estado);
        $stm->bindValue(':endereco',  $data->endereco);
        $stm->bindValue(':complemento',  $data->complemento);
        $stm->bindValue(':email',  $data->email);
        $stm->bindValue(':senha',  $data->senha);

 
        $stm->execute();
		$lastId = $db->lastInsertId();
		$result = $stm;
		
		if($result->rowCount()){
            $success = true;
        }else{
            $success = false;
        }
		
		echo json_encode(array(

            "data" => $result,
			"id_usuario" => $lastId, 
            "success" => true
            )
        );
    }
	
	public function deletar() {
        $data = (object) $_POST;

        $db = $this->getDb();
        $stm = $db->prepare('DELETE FROM Usuarios  WHERE id_usuario = :id_usuario');
        $stm->bindValue(':id_usuario', $data->id_usuario);
        $stm->execute();
        $result = $stm;
		
		if($result->rowCount()){
            $success = true;
        }else{
            $success = false;
        }

        echo json_encode(array(
            "data" => $result,
            "success" => true
            )
        );
    }
    public function alterarSenha() {
        $data = (object) $_POST;

        $db = $this->getDb();
        $stm = $db->prepare('UPDATE Usuarios SET senha = :senha where senha = :oldsenha '); // WHERE id_usuario = :id_usuario

        $stm->bindValue(':senha',  $data->senha);
		$stm->bindValue(':oldsenha',  $data->oldsenha);
        //$stm->bindValue(':id_usuario',  $data->id_usuario);
        
        $stm->execute();
		$result = $stm;
		
		if($result->rowCount()){
            $success = true;
        }else{
            $success = false;
        }
		
		echo json_encode(array(
            "data" => $result,
            "success" => true
            )
        );
    }
	
	public function alterar() {
        $data = (object) $_POST;

        $db = $this->getDb();
        $stm = $db->prepare('UPDATE Usuarios SET nome = :nome, email = :email, idade = :idade, endereco = :endereco, cep = :cep, complemento =:complemento, telefone =:telefone, cidade =:cidade, estado =:estado  WHERE id_usuario = :id_usuario');

        $stm->bindValue(':nome',  $data->nome);
        $stm->bindValue(':idade',  $data->idade);
        $stm->bindValue(':telefone',  $data->telefone);
        $stm->bindValue(':cidade',  $data->cidade);
        $stm->bindValue(':cep',  $data->cep);
        $stm->bindValue(':estado',  $data->estado);
        $stm->bindValue(':endereco',  $data->endereco);
        $stm->bindValue(':complemento',  $data->complemento);
        $stm->bindValue(':email',  $data->email);
        $stm->bindValue(':id_usuario',  $data->id_usuario);
        
        $stm->execute();
		$result = $stm;
		
		if($result->rowCount()){
            $success = true;
        }else{
            $success = false;
        }
		
		echo json_encode(array(
            "data" => $result,
            "success" => true
            )
        );
    }
}

$acao = $_POST["action"];

$aluno = new Aluno();
$aluno->$acao();
?>

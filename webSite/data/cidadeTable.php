<?php

session_start();

require_once 'Base.php';

class Cidade extends Base{

    public function inserir() {
        $data = (object) $_POST;

        $db = $this->getDb();
        $stm = $db->prepare('INSERT INTO Cidades (nome) values (:nome) ');

		$stm->bindValue(':nome',  $data->nome);

 
        $stm->execute();
		$lastId = $db->lastInsertId();
		$result = $stm;
		
		if($result->rowCount()){
            $success = true;
        }else{
            $success = false;
        }
		
		echo json_encode(array(

            "data" => $result,
			"id_cidade" => $lastId, 
            "success" => true
            )
        );
    }


    public function select() {
    $data = (object) $_POST;

    $db = $this->getDb();
    $stm = $db->prepare('SELECT * FROM Cidades');
 
    $stm->execute();
    $result = $stm->fetchAll( PDO::FETCH_ASSOC);



    echo json_encode(array(
        "data" => $result,
        "success" => true
        )
    );
    
    }

}

$acao = $_POST["action"];

$cidade = new Cidade();
$cidade->$acao();
?>
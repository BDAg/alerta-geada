<?php

require_once 'Base.php';

class Login extends Base{

    public function select() {
        $data = (object) $_POST;

        $db = $this->getDb();
        $stm = $db->prepare('SELECT * FROM Usuarios WHERE email = :email AND senha = :senha');
        $stm->bindValue(':email',    $data->login);
        $stm->bindValue(':senha', $data->pass);
        $stm->execute();
        $result = $stm->fetch( PDO::FETCH_ASSOC);

        if($result["id_usuario"]){
            $success = true;
            session_start();
            $_SESSION["id_usuario"] = $result["id_usuario"];
            $_SESSION["nome"] = $result["nome"];
            $_SESSION["email"]   = $data->login;
        }else{
            $success = false;
        }
        
        echo json_encode(array(
          "data" => $result,
          "success" => $success
        ));
    }
}

$acao = $_POST["action"];

$login = new Login();
$login->$acao();
?>

<h1>ALERTA GEADA</h1>

<p>Nosso projeto visa elaborar um sistema automatizado de alerta de possíveis geadas. </p>


<h2>INTEGRANTES ALERTA DE GEADA</h2>

• [GABRIEL DOS SANTOS SILVA](https://gitlab.com/BDAg/alerta-geada/wikis/Gabriel-dos-Santos-Silva)
    
• [GABRIEL NUNES HENKE](https://gitlab.com/BDAg/alerta-geada/wikis/Gabriel-Nunes-Henke)
    
• [GUILHERME BARBOSA BERALDO](https://gitlab.com/BDAg/alerta-geada/wikis/Guilherme-Barbosa-Beraldo)
    
• [JOÃO VITOR FALÉCO](https://gitlab.com/BDAg/alerta-geada/wikis/João-Vitor-Faléco)    
    
• [PEDRO PRATES MARIANO SANT’ANNA DE OLIVEIRA](https://gitlab.com/BDAg/alerta-geada/wikis/Pedro-Prates-Mariano-Sant'anna-de-Oliveira)
    
• [RAFAEL RODOLFO PINTO CHAVES](https://gitlab.com/BDAg/alerta-geada/wikis/Rafael-Rodolfo-Pinto-Chaves)
    
• [VICTOR RIBEIRO DE CAMARGO MACHADO](https://gitlab.com/BDAg/alerta-geada/wikis/Victor-Ribeiro-de-Camargo-Machado)
    

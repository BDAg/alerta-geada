class Object():
	def __init__(self,Requests):
		self.req = Requests
		self.r = ''
		
	def checkConnection(self,url):
		try:
			self.r = self.req.get(url,timeout=3)
			return 200
		except self.req.exceptions.RequestException as err:
			return 400
		except self.req.exceptions.HTTPError as errh:
			return 330
		except self.req.exceptions.ConnectionError as errc:
			return 400
		except self.req.exceptions.Timeout as errt:
			return 504
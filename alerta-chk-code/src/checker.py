class Checker():
	def __init__(self):
		self.umidade = ''
		self.velWind = ''
		self.temp = ''
		
	def setData(self,umidade,velWind,temp):
		if(umidade != 'null'):
			self.umidade = float(umidade)
			self.velWind = float(velWind) 
			self.temp = float(temp)
		
	def isWhiteFrost(self):
		if self.umidade != '' and self.velWind != '' and self.temp != '':
			if self.umidade > 1 and self.velWind < 100 and self.temp < 100: ##GEADA BRANCA 19 22 e 5
				return True
			else:
				return False;
		return False
		
	def isBlackFrost(self):
		if self.umidade != '' and self.velWind != '' and self.temp != '':
			if self.umidade < 1 and self.velWind > 1 and self.temp < 100: ##GEADA NEGRA 33 20 e 4
				return True 
			else:
				return False
		return False

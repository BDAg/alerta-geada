class Object():
	def __init__(self,Src):
		self.cidade = ''
		self.pais = ''
		self.estado = ''
		self.src = Src

		
	def setLocal(self,cidade,estado,pais):
		self.cidade = cidade
		self.pais = pais
		self.estado = estado
		
	def getCidade(self):
		return self.cidade 
		
	def getCidadeFormat(self):
		return self.src.remover_acentos(self,self.cidade).lower().capitalize()
	
	def getPais(self):
		return self.pais
		
	def getPaisFormat(self):
		return self.src.remover_acentos(self.pais).lower().capitalize()
		
	def getEstado(self):
		return self.estado
	
	def getEstadoFormat(self):
		return self.src.remover_acentos(self,self.estado).lower().capitalize()
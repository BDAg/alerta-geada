import mysql.connector as dblib
from mysql.connector import errorcode as decode
from datetime import datetime

class Resources():
	def __init__(self,dbobj):
		self.conn = dbobj('monty','251410123Le!','104.248.127.191','master',dblib,decode)
		self.cursor = self.conn.getCursor()
		self.cursor2 = self.conn.getCursor()
		self.cursor4 = self.conn.getCursor()
		self.cursor3 = self.conn.getCursor()
		self.idCidade = '' 
		self.now = ''
		self.idCity = ''
	def getDbCities(self):
		try:
			self.cursor.execute('select Cidades.nome,Fazendas.nome from Cidades join Fazendas on Cidades.id_cidade = Fazendas.id_cidade where Cidades.nome is not null')
			return self.cursor.fetchall()
		except:
			return 500
	
	def getEmailsByCity(self,cidade):
		try:
			self.cursor2.execute("select email from Fazendas join Cidades on Fazendas.id_cidade = Cidades.id_cidade join Usuarios on Fazendas.id_usuario = Usuarios.id_usuario where cidade = '" + cidade + "'")
			return self.cursor2.fetchall()
		except:
			return 500
			
	def getIdCity(self,cidade):
		try:
			self.cursor4.execute("select id_cidade from Cidades where nome = '" + cidade +  "'")
			return (self.cursor4.fetchone()[0])
		except:
			return 'NULL'
			
	def saveData(self,cidade,json):
		self.now = datetime.now().strftime('%Y-%m-%d %H:%M:')
		self.idCity = self.getIdCity(cidade)
		if self.idCity != 'NULL' and self.idCity != '':
			try:
				sql = "INSERT INTO Climas (datahora,maximo,minima,condicao_tempo,pressao,umidade,vento_vel,id_cidade,fonte) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"
				val = [
					(self.now, json['temperature']['max'], json['temperature']['min'],json['condicaoTempo'],json['pressao'],json['umidade'],json['velWind'],self.idCity,json['fonte'])
				]
				self.cursor3.executemany(sql,val)
				self.conn.getConnect().commit()
				return 200
			except ValueError:
				return 500
		else:
			return 404
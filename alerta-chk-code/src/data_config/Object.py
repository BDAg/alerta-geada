class Object():
	def __init__(self,user,password,host,database,dbconnect,dbdecode):
		self.config = {
			'user': user,
			'password': password,
			'host': host, 
			'database': database
		}
		try:
			self.conn = dbconnect.connect(**self.config)
		except dbconnect.Error as err:
			if err.errno == dbdecode.ER_ACCESS_DENIED_ERROR:
				print('Acesso negado data@senha incorretos')
			elif err.errno == dbdecode.ER_BAD_DB_ERROR:
				print('Banco de dados inexistente')
			else:
				print(err)
	
	def getCursor(self):
		return self.conn.cursor(buffered=True)
	
	def getConnect(self):
		return self.conn
	
	def getClose(self):
		self.conn.close()
import json
from unicodedata import normalize

class Source():
	def remover_acentos(self,txt):
		return normalize('NFKD',txt).encode('ASCII','ignore').decode('ASCII')
		
	def verifyNullFields(data):
		for key in data:
			if 'dict' in str(type(data[key])):
				for keyd in data['temperature']:
					if 'null' in data['temperature'][keyd]:
						return 404
			if 'null' in data[key]:
				return 404
class Person():
	def __init__(self,path,Json,Requests,BeautifulSoup,Src):
		self.token = '262bc192d12ce73baf265193eb892f95'
		self.bruteContent = 'null' 
		self.pathx = path
		self.json = Json
		self.req = Requests
		self.bf = BeautifulSoup
		self.cidade = '' 
		self.pais = ''
		self.src = Src
		self.data = {'umidade':'null',
					 'pressao':'null',
					 'velWind':'null',
					 'temperature' : {'min':'null',
					 				  'max':'null'
									 },
					 'condicaoTempo':'null',
					 'datahora':'null',
					 'cidade':'null',
					 'pais':'null',
					 'fonte':'null'
					 }
	
	def setLocal(self,cidade,pais):
		self.cidade = cidade 
		self.pais = pais
	
	def getContent(self,id):
		return self.bf(self.req.get('http://api.openweathermap.org/data/2.5/weather?id=' + str(id) + '&APPID=262bc192d12ce73baf265193eb892f95&mode=xml&units=metric&lang=pt').content,'html.parser')
						 
	def getId(self):
		for i in self.getCityList(self.pathx):
			if self.cidade == i['name'] and self.pais == i['country']:
				self.bruteContent = self.getContent(i['id'])

	def getCityList(self,path):
		self.jsonOpenArchive = open(path,encoding="utf-8")
		self.cityList = self.json.load(self.jsonOpenArchive)
		self.jsonOpenArchive.close
		return self.cityList
	
	def setData(self):
		self.getId()
		if self.bruteContent != 'null':
			if self.bruteContent.find('humidity'):
				self.data['umidade'] = self.bruteContent.find('humidity').get('value')
			if self.bruteContent.find('pressure'):
				self.data['pressao'] = self.bruteContent.find('pressure').get('value')
			if self.bruteContent.find('speed').get('value'):
				self.data['velWind'] = self.bruteContent.find('speed').get('value')
			if self.bruteContent.find('temperature'):
				self.data['temperature']['min'] = self.bruteContent.find('temperature').get('min')
				self.data['temperature']['max'] = self.bruteContent.find('temperature').get('max')
			if self.bruteContent.find('clouds'):
				self.data['condicaoTempo'] = self.bruteContent.find('clouds').get('name')
			if self.bruteContent.find('lastupdate'):
				self.data['datahora'] = self.bruteContent.find('lastupdate').get('value')
			if self.bruteContent.find('city'):
				self.data['cidade'] = self.bruteContent.find('city').get('name')
			if self.bruteContent.find('country'):
				self.data['pais'] = self.bruteContent.find('country').get_text('country')
				
			self.data['fonte'] = 'Open Weather Brasil'
			return 200;
		else:
			return 400;
	
	def getData(self):
		return self.data
	
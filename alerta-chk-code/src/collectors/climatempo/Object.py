class Climatempo():
	def __init__(self,Requests,Json):
		self.token = '2c13e733281e9a602302fffb77310e49'
		self.nomeCidade = ''
		self.ufCidade = ''
		self.req = Requests
		self.Json = Json
		self.bruteContent = '' 
		self.conteudox = ''
		self.conteudo = ''
		self.id = ''
		self.site = ''
		self.jsonPronto = ''
		self.result = {'umidade':'null',
					 'pressao':'null',
					 'velWind':'null',
					 'temperature' : {'min':'null',
					 				  'max':'null'
									 },
					 'condicaoTempo':'null',
					 'datahora':'null',
					 'cidade':'null',
					 'pais':'null',
					 'fonte':'null'
		}

		
	def setLocal(self,nomeCidade,ufCidade):
		self.nomeCidade = nomeCidade
		self.ufCidade = ufCidade
		
	def getId(self):
		self.bruteContent = self.req.get('http://apiadvisor.climatempo.com.br/api/v1/locale/city?name='+ self.nomeCidade + '&state=' + self.ufCidade + '&token=' + self.token).content
		self.conteudox = str(self.bruteContent)
		if 'id' in self.conteudox:
			return (self.conteudox[self.conteudox.find(':')+1:self.conteudox.find(',')])
		else:
			return 404

	def setData(self):
		self.id = self.getId()

		if self.id == 404:
			return 400
		else:
			self.site = self.req.get ('http://apiadvisor.climatempo.com.br/api/v1/weather/locale/'+ str(self.id) + '/current?token=' + self.token).content
			self.conteudo = self.site
			self.jsonPronto = self.Json.loads(self.conteudo.decode('utf-8'))
			self.result['temperature']['min'] = self.jsonPronto['data']['temperature']
			self.result['temperature']['max'] = self.jsonPronto['data']['temperature']
			self.result['umidade'] = self.jsonPronto['data']['humidity']
			self.result['datahora'] = self.jsonPronto['data']['date']
			self.result['condicaoTempo'] = self.jsonPronto['data']['condition']
			self.result['pressao'] = self.jsonPronto['data']['pressure']
			self.result['velWind'] = self.jsonPronto['data']['wind_velocity']
			self.result['cidade'] = self.jsonPronto['name']
			self.result['estado'] = self.jsonPronto['state']
			self.result['pais'] = self.jsonPronto['country']
			self.result['fonte'] = 'Climatempo'
			
			return 200

	def getData(self):
		return self.result
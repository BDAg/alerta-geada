import json
import requests
from bs4 import BeautifulSoup
from AcessToken import AcessToken
from src.Object import Object as global_obj
from src.collectors.climatempo.Object import Climatempo
from src.collectors.openweather.src.Source import Source as src
from src.collectors.openweather.src.Object import Person as obj
from src.collectiors_config.Object import Object as collectiors_config
from src.data_config.resources import Resources as srcdb
from src.data_config.Object import Object as dbobj
from src.checker import Checker as chk
class ImportLibs():

	def requests():
		return requests
		
	def json():
		return json
		
	def BeautifulSoup():
		return BeautifulSoup
		
	def global_obj():
		return global_obj(src)
		
	def obj():
		return obj('./src/collectors/openweather/src/city.list.json',json,requests,BeautifulSoup,src)
		
	def Climatempo():
		return Climatempo(requests,json)
		
	def collectiors_config():
		return collectiors_config(requests)
		
	def checkRequire():
		return AcessToken(requests,collectiors_config)
	
	def db():
		return srcdb(dbobj)
	
	def chk():
		return chk()
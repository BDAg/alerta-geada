from pathlibs import ImportLibs as libs
import time

chkClimaTempo = libs.chk()
chkOpenWeather = libs.chk()
db = libs.db()
objOpenWeather = libs.obj()
objGlobal = libs.global_obj()
objClimaTempo = libs.Climatempo()
dbCities = dict(db.getDbCities()).keys()

if dbCities == 500:
	pass
else:
	for i in dbCities:
		objGlobal.setLocal(i,'SP','BR')
		objClimaTempo.setLocal(objGlobal.getCidade(),objGlobal.getEstado())
		objOpenWeather.setLocal(objGlobal.getCidadeFormat(),objGlobal.getPais())
		getResultSet = objClimaTempo.setData()

		chkClimaTempo.setData(objClimaTempo.getData()['umidade'],
								objClimaTempo.getData()['velWind'],
								objClimaTempo.getData()['temperature']['min'])
		if getResultSet == 200:
			db.saveData(objGlobal.getCidade(),objClimaTempo.getData())
			
		if chkClimaTempo.isWhiteFrost() == True or chkClimaTempo.isBlackFrost == True :
			z = []
			z = list(db.getEmailsByCity(objGlobal.getCidade()))
			for i in range(0,len(z)):
				libs.requests().post('http://104.248.127.191/webSite/alerta/src/email.php',data={'email':z[i],'cidade': objGlobal.getCidade() })
			
		if objOpenWeather.setData() != 400:

			chkOpenWeather.setData(objOpenWeather.getData()['umidade'],
								  objOpenWeather.getData()['velWind'],
								  objOpenWeather.getData()['temperature']['min'])
		
			if chkOpenWeather.isWhiteFrost() == True or chkOpenWeather.isBlackFrost == True :
				z = []
				z = list(db.getEmailsByCity(objGlobal.getCidadeFormat()))
				for i in range(0,len(z)):
					libs.requests().post('http://104.248.127.191/webSite/alerta/src/email.php',data={'email':z[i],'cidade': objGlobal.getCidade() })
					
			
			db.saveData(objGlobal.getCidadeFormat(),objOpenWeather.getData())				

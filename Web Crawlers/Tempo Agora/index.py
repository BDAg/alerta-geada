import requests
from bs4 import BeautifulSoup
from lxml import html
from datetime import datetime

now = datetime.now() #puxando data hora do sistema


nomeCidade = input('Digite o nome da cidade: ')
ufCidade = input('Digite o UF da cidade: ')

req = requests.get ('http://www.tempoagora.com.br/previsao-do-tempo/'+ ufCidade +'/' + nomeCidade+ '/')

soup = BeautifulSoup(req.content, "lxml") #bs4

selecao = soup.find_all('span')

selecao2 = soup.find(id="main").find('p')

contador=0


print (60*'-')
print ("\nData e Hora:{}/{}/{}-{}:{}:{:.0f}".format((now.day), (now.month), (now.year), (now.hour), (now.minute), (now.second)))

for link in selecao:

	if contador ==5:
	 	print ("Temperatura:",link.get_text('class'))
	if contador ==9:
	 	print ("Velocidade do vento:",link.get_text('class'))
	if contador ==11:
	 	print ("Pressão:",link.get_text('class'))
	if contador ==13:
	 	print ("Umidade:",link.get_text('class'))

	contador = contador +1


print("Condição: " + selecao2.get_text() + "\n\n")
print("Fonte: Tempo Agora")

print (60*'-')

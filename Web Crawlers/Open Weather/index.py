# -- Coding: UTF-8 --
import requests
from bs4 import BeautifulSoup #tratamento de HTML

site = requests.get('http://api.openweathermap.org/data/2.5/weather?id=3453242&APPID=262bc192d12ce73baf265193eb892f95&mode=xml&units=metric&lang=pt').content #atribuir resposta da requisição do site
#(.content esta buscando o conteudo do site solicitado pelo request)

toConvert = BeautifulSoup(site, 'html.parser') #variavel convertendo a requisição para HTML novamente

umidade = toConvert.find('humidity').get('value')
pressao = toConvert.find('pressure').get('value')
velWind = (toConvert.find('speed').get('value'))
minima = toConvert.find('temperature').get('min')
maxima = toConvert.find('temperature').get('max')
condicaoTempo = toConvert.find('clouds').get('name')
datahora = toConvert.find('lastupdate').get('value')
cidade = toConvert.find('city').get('name')
pais = toConvert.find('country').get_text('country')
fonte = 'Open Weather Brasil'

print('='*35)
print('Cidade: {}-{} \nData e Hora: {} \nTemperatura Maxima: {}ºC \nTemperatura Minima: {}ºC \nUmidade: {} \nPressao Atmosferica: {} \nVelocidade do vento: {} \nCondicao do tempo: {}'.format(cidade, pais, datahora, maxima, minima, umidade, pressao, velWind, condicaoTempo))
print('Fonte: {}'.format(fonte))
print('='*35)
